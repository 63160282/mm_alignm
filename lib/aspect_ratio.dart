import 'package:flutter/material.dart';

void main() {
  runApp(const AspectRatioExample());
}

class AspectRatioExample extends StatelessWidget {
  const AspectRatioExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 200,
        width: 300,
        color: Colors.green[200],
        child: const FlutterLogo(),
      ),
    );
  }
}
